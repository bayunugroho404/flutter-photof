import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'package:photof/network/response/response_add_image.dart';
import 'package:photof/network/response/response_message.dart';
import 'package:photof/network/response/response_update_avatar.dart';
import 'package:photof/utils/base_url.dart';

class ApiServiceImage {
  final String _addImage = endpoint("photo/add");
  final String _avatar = endpoint("avatar");

  Future<ResponseAddImage> addPhoto(
      String token, PickedFile image, String user_id) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request = http.MultipartRequest("POST", Uri.parse("$_addImage"));
    request.headers.addAll(headers);
    request.fields["user_id"] = user_id;
    var pic = await http.MultipartFile.fromPath("photo", image.path);
    request.files.add(pic);
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print("ssadsad "+responseString.toString());
    if (response.statusCode == 200) {
      return ResponseAddImage.fromJson(json.decode(responseString));
    } else {
      throw Exception('gagal');
    }
  }



  Future<ResponseUpdateAvatar> updateAvatar(
      String token, PickedFile image) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request = http.MultipartRequest("POST", Uri.parse("$_avatar"));
    request.headers.addAll(headers);
    var pic = await http.MultipartFile.fromPath("avatar", image.path);
    request.files.add(pic);
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    if (response.statusCode == 200) {
      return ResponseUpdateAvatar.fromJson(json.decode(responseString));
    } else {
      throw Exception('gagal');
    }
  }

}
