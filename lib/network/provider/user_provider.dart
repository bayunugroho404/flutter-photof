import 'dart:io';

import 'package:dio/dio.dart';
import 'package:photof/network/response/default_response.dart';
import 'package:photof/network/response/login_response.dart';
import 'package:photof/network/response/profile_response.dart';
import 'package:photof/utils/base_url.dart';

class UserProvider{
  final String _login = endpoint("auth/login");
  final String _register = endpoint("auth/register");
  final String _profile = endpoint("user");

  Dio _dio;

  UserProvider(){
    BaseOptions baseOptions = BaseOptions(
        receiveTimeout: 50000,
        connectTimeout: 50000,
        contentType: Headers.formUrlEncodedContentType);
    _dio = Dio(baseOptions);
  }

  Future<LoginResponse> login(email, password) async {
    try {
      Response response = await _dio.post(_login, data: {
        "email" : email,
        "password": password
      });
      print(response.data.toString());
      return LoginResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<DefaultResponse> register(name, email, password, passwordConfirmation, phoneNumber,is_user) async {
    try {
      Response response = await _dio.post(_register, data: {
        "name" : name,
        "email" : email,
        "password": password,
        "password_confirmation": passwordConfirmation,
        "phone_number": phoneNumber,
        "is_user": is_user
      });
      return DefaultResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<ProfileResponse> profile(authToken) async {
    try {
      Response response = await _dio.get(_profile, options: Options(
          headers: {HttpHeaders.authorizationHeader: 'Bearer $authToken'}));
      return ProfileResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<DefaultResponse> registerPhotof(name, email, password, passwordConfirmation, phoneNumber,is_user,category_id) async {
    try {
      Response response = await _dio.post(_register, data: {
        "name" : name,
        "email" : email,
        "password": password,
        "password_confirmation": passwordConfirmation,
        "phone_number": phoneNumber,
        "is_user": is_user,
        "category_id": category_id
      });
      return DefaultResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return null;
    }
  }
  String _handleError(DioError error) {
    String errorDescription = "";
    if (error is DioError) {
      switch (error.type) {
        case DioErrorType.cancel:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.connectTimeout:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.other:
          errorDescription =
          "Connection to API server failed due to internet connection";
          break;
        case DioErrorType.receiveTimeout:
          errorDescription = "Receive timeout in connection with API server";
          break;
        case DioErrorType.response:
          errorDescription =
          "Received invalid status code: ${error.response.statusCode}";
          break;
        case DioErrorType.sendTimeout:
          break;
      }
    } else {
      errorDescription = "Unexpected error occured";
    }
    return errorDescription;
  }
}