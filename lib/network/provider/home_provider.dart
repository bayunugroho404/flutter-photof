import 'dart:io';

import 'package:dio/dio.dart';
import 'package:photof/network/response/reponse_get_photo_by_photof.dart';
import 'package:photof/network/response/response_get_category.dart';
import 'package:photof/network/response/response_get_order_user.dart';
import 'package:photof/network/response/response_get_photof_by_category.dart';
import 'package:photof/network/response/response_get_review.dart';
import 'package:photof/network/response/response_message.dart';
import 'package:photof/network/response/response_photografer.dart';
import 'package:photof/network/response/response_post_review.dart';
import 'package:photof/utils/base_url.dart';

class HomeProvider {
  final String _getCategoty = endpoint("category/index");
  final String _getPhotografer = endpoint("get_photografer");
  final String _getPhotofCategoty = endpoint("photo/getPhotofByCategory");
  final String _postReview = endpoint("review/add");
  final String _getReviewByUserId = endpoint("review/get_review_by_user_id");
  final String _getReviewByPhotografer =
      endpoint("review/get_review_by_photografer");
  final String _getPhotofPhotof = endpoint("photo/get_photo_by_user_id");
  final String _getOrderByUser = endpoint("order/get_order_by_user_id");
  final String _getOrderByPhotog = endpoint("order/get_order_by_photof");
  final String _postOrder = endpoint("order/add");
  final String _updateOrder = endpoint("order/status");
  final String _updateProfile = endpoint("profile");
  final String _updateBio = endpoint("bio");

  Dio _dio;

  HomeProvider() {
    BaseOptions baseOptions = BaseOptions(
        receiveTimeout: 50000,
        connectTimeout: 50000,
        contentType: Headers.formUrlEncodedContentType);
    _dio = Dio(baseOptions);
  }

  Future<ResponseGetPhotoByCategory> getPhotoByCategory(
      category_id, token) async {
    try {
      Response response = await _dio.post(_getPhotofCategoty,
          data: {
            "id": category_id,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseGetPhotoByCategory.fromJson(response.data);
    } catch (error) {
      throw Exception('$error');
    }
  }

  Future<ResponsePostReview> postReview(
    String token,
    String rate,
    String user_id,
    String description,
    String photografer_id,
  ) async {
    try {
      Response response = await _dio.post(_postReview,
          data: {
            "rate": rate,
            "user_id": user_id,
            "description": description,
            "photografer_id": photografer_id
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponsePostReview.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseGetReview> getReviewPhotof(
    String token,
    String photografer_id,
  ) async {
    try {
      Response response = await _dio.post(_getReviewByPhotografer,
          data: {"photografer_id": photografer_id},
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseGetReview.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseGetReview> getReviewUser(
    String token,
    String user_id,
  ) async {
    try {
      Response response = await _dio.post(_getReviewByUserId,
          data: {"user_id": user_id},
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseGetReview.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseGetPhotoByPhotof> getPhotoByPhotof(user_id, token) async {
    try {
      Response response = await _dio.post(_getPhotofPhotof,
          data: {
            "user_id": user_id,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseGetPhotoByPhotof.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseGetOrderUser> getOrderUser(user_id, token) async {
    try {
      Response response = await _dio.post(_getOrderByUser,
          data: {
            "user_id": user_id,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseGetOrderUser.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseGetOrderUser> getOrderPhotog(photografer_id, token) async {
    try {
      Response response = await _dio.post(_getOrderByPhotog,
          data: {
            "photografer_id": photografer_id,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseGetOrderUser.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseMessage> updateOrder(order_id, status, token) async {
    try {
      Response response = await _dio.post(_updateOrder,
          data: {
            "order_id": order_id,
            "status": status,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseMessage.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseMessage> updateBio(description, token) async {
    try {
      Response response = await _dio.post(_updateBio,
          data: {
            "description": description,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseMessage.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseMessage> updateProfile(name, no, email, token) async {
    try {
      Response response = await _dio.post(_updateProfile,
          data: {
            "name": name,
            "no": no,
            "email": email,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseMessage.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseMessage> addOrder(
      user_id, date, packet, note, photografer_id, token) async {
    try {
      Response response = await _dio.post(_postOrder,
          data: {
            "user_id": user_id,
            "date": date,
            "packet": packet,
            "photografer_id": photografer_id,
            "note": note,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return ResponseMessage.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<ResponseGetCategory> getCategory() async {
    try {
      Response response = await _dio.get(_getCategoty);
      return ResponseGetCategory.fromJson(response.data);
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<ResponsePhotografer> getPhotografer(authToken) async {
    try {
      Response response = await _dio.get(_getPhotografer,
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $authToken'}));
      return ResponsePhotografer.fromJson(response.data);
    } catch (error) {
      throw Exception('$error');
    }
  }
}
