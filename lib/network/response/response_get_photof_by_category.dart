// To parse this JSON data, do
//
//     final responseGetPhotoByCategory = responseGetPhotoByCategoryFromJson(jsonString);

import 'dart:convert';

ResponseGetPhotoByCategory responseGetPhotoByCategoryFromJson(String str) => ResponseGetPhotoByCategory.fromJson(json.decode(str));

String responseGetPhotoByCategoryToJson(ResponseGetPhotoByCategory data) => json.encode(data.toJson());

class ResponseGetPhotoByCategory {
  ResponseGetPhotoByCategory({
    this.statusCode,
    this.photografer,
  });

  int statusCode;
  List<Photografer> photografer;

  factory ResponseGetPhotoByCategory.fromJson(Map<String, dynamic> json) => ResponseGetPhotoByCategory(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    photografer: json["photografer"] == null ? null : List<Photografer>.from(json["photografer"].map((x) => Photografer.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "photografer": photografer == null ? null : List<dynamic>.from(photografer.map((x) => x.toJson())),
  };
}

class Photografer {
  Photografer({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.categoryId,
    this.description,
    this.lat,
    this.lng,
    this.emailVerifiedAt,
    this.photo,
    this.click,
    this.createdAt,
    this.updatedAt,
    this.photos,
  });

  int id;
  String name;
  String phoneNumber;
  String email;
  String isUser;
  String workSchedule;
  String categoryId;
  String description;
  dynamic lat;
  dynamic lng;
  dynamic emailVerifiedAt;
  String photo;
  String click;
  DateTime createdAt;
  DateTime updatedAt;
  List<Photo> photos;

  factory Photografer.fromJson(Map<String, dynamic> json) => Photografer(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    email: json["email"] == null ? null : json["email"],
    isUser: json["is_user"] == null ? null : json["is_user"],
    workSchedule: json["work_schedule"] == null ? null : json["work_schedule"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"],
    lng: json["lng"],
    emailVerifiedAt: json["email_verified_at"],
    photo: json["photo"] == null ? null : json["photo"],
    click: json["click"] == null ? null : json["click"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    photos: json["photos"] == null ? null : List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "email": email == null ? null : email,
    "is_user": isUser == null ? null : isUser,
    "work_schedule": workSchedule == null ? null : workSchedule,
    "category_id": categoryId == null ? null : categoryId,
    "description": description == null ? null : description,
    "lat": lat,
    "lng": lng,
    "email_verified_at": emailVerifiedAt,
    "photo": photo == null ? null : photo,
    "click": click == null ? null : click,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "photos": photos == null ? null : List<dynamic>.from(photos.map((x) => x.toJson())),
  };
}

class Photo {
  Photo({
    this.id,
    this.userId,
    this.photo,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String userId;
  String photo;
  DateTime createdAt;
  DateTime updatedAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
    id: json["id"] == null ? null : json["id"],
    userId: json["user_id"] == null ? null : json["user_id"],
    photo: json["photo"] == null ? null : json["photo"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_id": userId == null ? null : userId,
    "photo": photo == null ? null : photo,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}
