class DefaultResponse {
  DefaultResponse({
    this.statusCode,
    this.message,
  });

  int statusCode;
  String message;

  factory DefaultResponse.fromJson(Map<String, dynamic> json) => DefaultResponse(
    statusCode: json["status_code"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode,
    "message": message,
  };
}
