// To parse this JSON data, do
//
//     final responseGetOrderUser = responseGetOrderUserFromJson(jsonString);

import 'dart:convert';

ResponseGetOrderUser responseGetOrderUserFromJson(String str) => ResponseGetOrderUser.fromJson(json.decode(str));

String responseGetOrderUserToJson(ResponseGetOrderUser data) => json.encode(data.toJson());

class ResponseGetOrderUser {
  ResponseGetOrderUser({
    this.statusCode,
    this.orders,
  });

  int statusCode;
  List<Order> orders;

  factory ResponseGetOrderUser.fromJson(Map<String, dynamic> json) => ResponseGetOrderUser(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    orders: json["orders"] == null ? null : List<Order>.from(json["orders"].map((x) => Order.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "orders": orders == null ? null : List<dynamic>.from(orders.map((x) => x.toJson())),
  };
}

class Order {
  Order({
    this.id,
    this.date,
    this.packet,
    this.userId,
    this.status,
    this.photograferId,
    this.note,
    this.createdAt,
    this.updatedAt,
    this.user,
    this.photografer,
  });

  int id;
  String date;
  String packet;
  String userId;
  String status;
  String photograferId;
  String note;
  DateTime createdAt;
  DateTime updatedAt;
  Photografer user;
  Photografer photografer;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
    id: json["id"] == null ? null : json["id"],
    date: json["date"] == null ? null : json["date"],
    packet: json["packet"] == null ? null : json["packet"],
    userId: json["user_id"] == null ? null : json["user_id"],
    status: json["status"] == null ? null : json["status"],
    photograferId: json["photografer_id"] == null ? null : json["photografer_id"],
    note: json["note"] == null ? null : json["note"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    user: json["user"] == null ? null : Photografer.fromJson(json["user"]),
    photografer: json["photografer"] == null ? null : Photografer.fromJson(json["photografer"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "date": date == null ? null : date,
    "packet": packet == null ? null : packet,
    "user_id": userId == null ? null : userId,
    "status": status == null ? null : status,
    "photografer_id": photograferId == null ? null : photograferId,
    "note": note == null ? null : note,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "user": user == null ? null : user.toJson(),
    "photografer": photografer == null ? null : photografer.toJson(),
  };
}

class Photografer {
  Photografer({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.categoryId,
    this.description,
    this.lat,
    this.lng,
    this.emailVerifiedAt,
    this.photo,
    this.click,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  Name name;
  String phoneNumber;
  Email email;
  String isUser;
  WorkSchedule workSchedule;
  String categoryId;
  Description description;
  dynamic lat;
  dynamic lng;
  dynamic emailVerifiedAt;
  String photo;
  String click;
  DateTime createdAt;
  DateTime updatedAt;

  factory Photografer.fromJson(Map<String, dynamic> json) => Photografer(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : nameValues.map[json["name"]],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    email: json["email"] == null ? null : emailValues.map[json["email"]],
    isUser: json["is_user"] == null ? null : json["is_user"],
    workSchedule: json["work_schedule"] == null ? null : workScheduleValues.map[json["work_schedule"]],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    description: json["description"] == null ? null : descriptionValues.map[json["description"]],
    lat: json["lat"],
    lng: json["lng"],
    emailVerifiedAt: json["email_verified_at"],
    photo: json["photo"] == null ? null : json["photo"],
    click: json["click"] == null ? null : json["click"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : nameValues.reverse[name],
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "email": email == null ? null : emailValues.reverse[email],
    "is_user": isUser == null ? null : isUser,
    "work_schedule": workSchedule == null ? null : workScheduleValues.reverse[workSchedule],
    "category_id": categoryId == null ? null : categoryId,
    "description": description == null ? null : descriptionValues.reverse[description],
    "lat": lat,
    "lng": lng,
    "email_verified_at": emailVerifiedAt,
    "photo": photo == null ? null : photo,
    "click": click == null ? null : click,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}

enum Description { I_HAVE_5_YEARS_OF_EXPERIENCE_AS_A_PHOTOGRAPHER, IM_PHOTOGRAPHER_PROFFESIONAL }

final descriptionValues = EnumValues({
  "Im photographer proffesional": Description.IM_PHOTOGRAPHER_PROFFESIONAL,
  "I have 5 years of experience as a photographer": Description.I_HAVE_5_YEARS_OF_EXPERIENCE_AS_A_PHOTOGRAPHER
});

enum Email { PHOTOF2_GMAIL_COM, PHOTOF1_GMAIL_COM }

final emailValues = EnumValues({
  "photof1@gmail.com": Email.PHOTOF1_GMAIL_COM,
  "photof2@gmail.com": Email.PHOTOF2_GMAIL_COM
});

enum Name { PHOTOF, ATUNG }

final nameValues = EnumValues({
  "atung": Name.ATUNG,
  "photof": Name.PHOTOF
});

enum WorkSchedule { EVERYDAY, SENIN_JUMAT }

final workScheduleValues = EnumValues({
  "everyday": WorkSchedule.EVERYDAY,
  "senin - jumat": WorkSchedule.SENIN_JUMAT
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
