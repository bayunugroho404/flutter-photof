// To parse this JSON data, do
//
//     final profileResponse = profileResponseFromJson(jsonString);

import 'dart:convert';

ProfileResponse profileResponseFromJson(String str) => ProfileResponse.fromJson(json.decode(str));

String profileResponseToJson(ProfileResponse data) => json.encode(data.toJson());

class ProfileResponse {
  ProfileResponse({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.description,
    this.lat,
    this.lng,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  String phoneNumber;
  String email;
  int isUser;
  dynamic workSchedule;
  dynamic description;
  dynamic lat;
  dynamic lng;
  dynamic emailVerifiedAt;
  DateTime createdAt;
  DateTime updatedAt;

  factory ProfileResponse.fromJson(Map<String, dynamic> json) => ProfileResponse(
    id: json["id"],
    name: json["name"],
    phoneNumber: json["phone_number"],
    email: json["email"],
    isUser: json["is_user"],
    workSchedule: json["work_schedule"],
    description: json["description"],
    lat: json["lat"],
    lng: json["lng"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "phone_number": phoneNumber,
    "email": email,
    "is_user": isUser,
    "work_schedule": workSchedule,
    "description": description,
    "lat": lat,
    "lng": lng,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
