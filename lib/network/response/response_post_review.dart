// To parse this JSON data, do
//
//     final responsePostReview = responsePostReviewFromJson(jsonString);

import 'dart:convert';

ResponsePostReview responsePostReviewFromJson(String str) => ResponsePostReview.fromJson(json.decode(str));

String responsePostReviewToJson(ResponsePostReview data) => json.encode(data.toJson());

class ResponsePostReview {
  ResponsePostReview({
    this.statusCode,
    this.reviews,
  });

  int statusCode;
  String reviews;

  factory ResponsePostReview.fromJson(Map<String, dynamic> json) => ResponsePostReview(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    reviews: json["reviews"] == null ? null : json["reviews"],
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "reviews": reviews == null ? null : reviews,
  };
}
