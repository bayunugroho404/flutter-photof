// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.statusCode,
    this.accessToken,
    this.user,
    this.tokenType,
  });

  int statusCode;
  String accessToken;
  User user;
  String tokenType;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    accessToken: json["access_token"] == null ? null : json["access_token"],
    user: json["user"] == null ? null : User.fromJson(json["user"]),
    tokenType: json["token_type"] == null ? null : json["token_type"],
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "access_token": accessToken == null ? null : accessToken,
    "user": user == null ? null : user.toJson(),
    "token_type": tokenType == null ? null : tokenType,
  };
}

class User {
  User({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.categoryId,
    this.description,
    this.lat,
    this.lng,
    this.emailVerifiedAt,
    this.photo,
    this.click,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  String phoneNumber;
  String email;
  String isUser;
  String workSchedule;
  String categoryId;
  String description;
  dynamic lat;
  dynamic lng;
  dynamic emailVerifiedAt;
  String photo;
  String click;
  DateTime createdAt;
  DateTime updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    email: json["email"] == null ? null : json["email"],
    isUser: json["is_user"] == null ? null : json["is_user"],
    workSchedule: json["work_schedule"] == null ? null : json["work_schedule"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"],
    lng: json["lng"],
    emailVerifiedAt: json["email_verified_at"],
    photo: json["photo"] == null ? null : json["photo"],
    click: json["click"] == null ? null : json["click"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "email": email == null ? null : email,
    "is_user": isUser == null ? null : isUser,
    "work_schedule": workSchedule == null ? null : workSchedule,
    "category_id": categoryId == null ? null : categoryId,
    "description": description == null ? null : description,
    "lat": lat,
    "lng": lng,
    "email_verified_at": emailVerifiedAt,
    "photo": photo == null ? null : photo,
    "click": click == null ? null : click,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}
