
// To parse this JSON data, do
//
//     final responseMessage = responseMessageFromJson(jsonString);

import 'dart:convert';

ResponseMessage responseMessageFromJson(String str) => ResponseMessage.fromJson(json.decode(str));

String responseMessageToJson(ResponseMessage data) => json.encode(data.toJson());

class ResponseMessage {
  ResponseMessage({
    this.statusCode,
    this.message,
  });

  int statusCode;
  String message;

  factory ResponseMessage.fromJson(Map<String, dynamic> json) => ResponseMessage(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
  };
}
