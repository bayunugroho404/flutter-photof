

import 'dart:convert';

ResponseGetCategory responseGetCategoryFromJson(String str) => ResponseGetCategory.fromJson(json.decode(str));

String responseGetCategoryToJson(ResponseGetCategory data) => json.encode(data.toJson());

class ResponseGetCategory {
  ResponseGetCategory({
    this.statusCode,
    this.data,
  });

  int statusCode;
  List<Datum> data;

  factory ResponseGetCategory.fromJson(Map<String, dynamic> json) => ResponseGetCategory(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  DateTime createdAt;
  DateTime updatedAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}
