// To parse this JSON data, do
//
//     final responseAddImage = responseAddImageFromJson(jsonString);

import 'dart:convert';

ResponseAddImage responseAddImageFromJson(String str) => ResponseAddImage.fromJson(json.decode(str));

String responseAddImageToJson(ResponseAddImage data) => json.encode(data.toJson());

class ResponseAddImage {
  ResponseAddImage({
    this.success,
    this.message,
    this.file,
  });

  bool success;
  String message;
  String file;

  factory ResponseAddImage.fromJson(Map<String, dynamic> json) => ResponseAddImage(
    success: json["success"] == null ? null : json["success"],
    message: json["message"] == null ? null : json["message"],
    file: json["file"] == null ? null : json["file"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "message": message == null ? null : message,
    "file": file == null ? null : file,
  };
}
