// To parse this JSON data, do
//
//     final responseGetReview = responseGetReviewFromJson(jsonString);

import 'dart:convert';

ResponseGetReview responseGetReviewFromJson(String str) => ResponseGetReview.fromJson(json.decode(str));

String responseGetReviewToJson(ResponseGetReview data) => json.encode(data.toJson());

class ResponseGetReview {
  ResponseGetReview({
    this.statusCode,
    this.reviews,
  });

  int statusCode;
  List<Review> reviews;

  factory ResponseGetReview.fromJson(Map<String, dynamic> json) => ResponseGetReview(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    reviews: json["reviews"] == null ? null : List<Review>.from(json["reviews"].map((x) => Review.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "reviews": reviews == null ? null : List<dynamic>.from(reviews.map((x) => x.toJson())),
  };
}

class Review {
  Review({
    this.id,
    this.rate,
    this.userId,
    this.description,
    this.photograferId,
    this.createdAt,
    this.updatedAt,
    this.user,
    this.photografer,
  });

  int id;
  String rate;
  String userId;
  String description;
  String photograferId;
  DateTime createdAt;
  DateTime updatedAt;
  Photografer user;
  Photografer photografer;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
    id: json["id"] == null ? null : json["id"],
    rate: json["rate"] == null ? null : json["rate"],
    userId: json["user_id"] == null ? null : json["user_id"],
    description: json["description"] == null ? null : json["description"],
    photograferId: json["photografer_id"] == null ? null : json["photografer_id"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    user: json["user"] == null ? null : Photografer.fromJson(json["user"]),
    photografer: json["photografer"] == null ? null : Photografer.fromJson(json["photografer"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "rate": rate == null ? null : rate,
    "user_id": userId == null ? null : userId,
    "description": description == null ? null : description,
    "photografer_id": photograferId == null ? null : photograferId,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "user": user == null ? null : user.toJson(),
    "photografer": photografer == null ? null : photografer.toJson(),
  };
}

class Photografer {
  Photografer({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.categoryId,
    this.description,
    this.lat,
    this.lng,
    this.emailVerifiedAt,
    this.photo,
    this.click,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  String phoneNumber;
  String email;
  String isUser;
  String workSchedule;
  String categoryId;
  String description;
  dynamic lat;
  dynamic lng;
  dynamic emailVerifiedAt;
  String photo;
  String click;
  DateTime createdAt;
  DateTime updatedAt;

  factory Photografer.fromJson(Map<String, dynamic> json) => Photografer(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    email: json["email"] == null ? null : json["email"],
    isUser: json["is_user"] == null ? null : json["is_user"],
    workSchedule: json["work_schedule"] == null ? null : json["work_schedule"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"],
    lng: json["lng"],
    emailVerifiedAt: json["email_verified_at"],
    photo: json["photo"] == null ? null : json["photo"],
    click: json["click"] == null ? null : json["click"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "email": email == null ? null : email,
    "is_user": isUser == null ? null : isUser,
    "work_schedule": workSchedule == null ? null : workSchedule,
    "category_id": categoryId == null ? null : categoryId,
    "description": description == null ? null : description,
    "lat": lat,
    "lng": lng,
    "email_verified_at": emailVerifiedAt,
    "photo": photo == null ? null : photo,
    "click": click == null ? null : click,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}
