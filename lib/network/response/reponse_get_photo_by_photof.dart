// To parse this JSON data, do
//
//     final responseGetPhotoByPhotof = responseGetPhotoByPhotofFromJson(jsonString);

import 'dart:convert';

ResponseGetPhotoByPhotof responseGetPhotoByPhotofFromJson(String str) => ResponseGetPhotoByPhotof.fromJson(json.decode(str));

String responseGetPhotoByPhotofToJson(ResponseGetPhotoByPhotof data) => json.encode(data.toJson());

class ResponseGetPhotoByPhotof {
  ResponseGetPhotoByPhotof({
    this.statusCode,
    this.photo,
  });

  int statusCode;
  List<Photo> photo;

  factory ResponseGetPhotoByPhotof.fromJson(Map<String, dynamic> json) => ResponseGetPhotoByPhotof(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    photo: json["photo"] == null ? null : List<Photo>.from(json["photo"].map((x) => Photo.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "photo": photo == null ? null : List<dynamic>.from(photo.map((x) => x.toJson())),
  };
}

class Photo {
  Photo({
    this.id,
    this.userId,
    this.photo,
    this.createdAt,
    this.updatedAt,
    this.user,
  });

  int id;
  int userId;
  String photo;
  DateTime createdAt;
  DateTime updatedAt;
  User user;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
    id: json["id"] == null ? null : json["id"],
    userId: json["user_id"] == null ? null : json["user_id"],
    photo: json["photo"] == null ? null : json["photo"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    user: json["user"] == null ? null : User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_id": userId == null ? null : userId,
    "photo": photo == null ? null : photo,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "user": user == null ? null : user.toJson(),
  };
}

class User {
  User({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.categoryId,
    this.description,
    this.lat,
    this.lng,
    this.emailVerifiedAt,
    this.photo,
    this.click,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  String phoneNumber;
  String email;
  int isUser;
  String workSchedule;
  int categoryId;
  String description;
  dynamic lat;
  dynamic lng;
  dynamic emailVerifiedAt;
  String photo;
  int click;
  DateTime createdAt;
  DateTime updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    email: json["email"] == null ? null : json["email"],
    isUser: json["is_user"] == null ? null : json["is_user"],
    workSchedule: json["work_schedule"] == null ? null : json["work_schedule"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"],
    lng: json["lng"],
    emailVerifiedAt: json["email_verified_at"],
    photo: json["photo"] == null ? null : json["photo"],
    click: json["click"] == null ? null : json["click"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "email": email == null ? null : email,
    "is_user": isUser == null ? null : isUser,
    "work_schedule": workSchedule == null ? null : workSchedule,
    "category_id": categoryId == null ? null : categoryId,
    "description": description == null ? null : description,
    "lat": lat,
    "lng": lng,
    "email_verified_at": emailVerifiedAt,
    "photo": photo == null ? null : photo,
    "click": click == null ? null : click,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}
