// To parse this JSON data, do
//
//     final responsePhotografer = responsePhotograferFromJson(jsonString);

import 'dart:convert';

ResponsePhotografer responsePhotograferFromJson(String str) => ResponsePhotografer.fromJson(json.decode(str));

String responsePhotograferToJson(ResponsePhotografer data) => json.encode(data.toJson());

class ResponsePhotografer {
  ResponsePhotografer({
    this.statusCode,
    this.photo,
  });

  int statusCode;
  List<ResponsePhotograferPhoto> photo;

  factory ResponsePhotografer.fromJson(Map<String, dynamic> json) => ResponsePhotografer(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    photo: json["photo"] == null ? null : List<ResponsePhotograferPhoto>.from(json["photo"].map((x) => ResponsePhotograferPhoto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "photo": photo == null ? null : List<dynamic>.from(photo.map((x) => x.toJson())),
  };
}

class ResponsePhotograferPhoto {
  ResponsePhotograferPhoto({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.categoryId,
    this.description,
    this.lat,
    this.lng,
    this.emailVerifiedAt,
    this.photo,
    this.createdAt,
    this.updatedAt,
    this.photos,
  });

  int id;
  String name;
  String phoneNumber;
  String email;
  int isUser;
  String workSchedule;
  int categoryId;
  String description;
  dynamic lat;
  dynamic lng;
  dynamic emailVerifiedAt;
  String photo;
  DateTime createdAt;
  DateTime updatedAt;
  List<PhotoPhoto> photos;

  factory ResponsePhotograferPhoto.fromJson(Map<String, dynamic> json) => ResponsePhotograferPhoto(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    email: json["email"] == null ? null : json["email"],
    isUser: json["is_user"] == null ? null : json["is_user"],
    workSchedule: json["work_schedule"] == null ? null : json["work_schedule"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"],
    lng: json["lng"],
    emailVerifiedAt: json["email_verified_at"],
    photo: json["photo"] == null ? null : json["photo"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    photos: json["photos"] == null ? null : List<PhotoPhoto>.from(json["photos"].map((x) => PhotoPhoto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "email": email == null ? null : email,
    "is_user": isUser == null ? null : isUser,
    "work_schedule": workSchedule == null ? null : workSchedule,
    "category_id": categoryId == null ? null : categoryId,
    "description": description == null ? null : description,
    "lat": lat,
    "lng": lng,
    "email_verified_at": emailVerifiedAt,
    "photo": photo == null ? null : photo,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "photos": photos == null ? null : List<dynamic>.from(photos.map((x) => x.toJson())),
  };
}

class PhotoPhoto {
  PhotoPhoto({
    this.id,
    this.userId,
    this.photo,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int userId;
  String photo;
  DateTime createdAt;
  DateTime updatedAt;

  factory PhotoPhoto.fromJson(Map<String, dynamic> json) => PhotoPhoto(
    id: json["id"] == null ? null : json["id"],
    userId: json["user_id"] == null ? null : json["user_id"],
    photo: json["photo"] == null ? null : json["photo"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_id": userId == null ? null : userId,
    "photo": photo == null ? null : photo,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}
