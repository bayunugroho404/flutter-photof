

String endpoint(String url) {
  String baseUrl = "";
  final buildVariant = "production";
  if (buildVariant == "dev") {
    baseUrl = "http://192.168.1.12:8000/api/";
  } else {
    baseUrl = "https://photof.umkmmeruyung.my.id/api/";
  }
  return baseUrl + url;
}
