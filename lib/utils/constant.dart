import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


//file constant to be accessible in many classes
const Color primary = Colors.grey;
const Color secondary = Color(0xffc8c7c6);
const Color white = Color(0xffeeecec);
const Color grey = Color(0xff998869);
const String PAKET1 = "Paket 1\n-1 Hari Dokumentasi\n-2 Fotografer\n-1 Videografer\n-1 Album Foto Landsacpe 20x30cm (10 Sheet/20 Halaman) \n+ Box Custom dan Tote Bag1 DVD File Foto (Soft Copy)\n-1 DVD File Video yang sudah di edit 10 – 30 Menit Full HD";
const String PAKET2 = "-1 Hari Dokumentasi\n-3 Fotografer\n-2 Videografer\n-2 Album Foto Landsacpe 20x30cm (10 Sheet/20 Halaman) \n+ Box Custom dan Tote Bag1 DVD File Foto (Soft Copy)\n-1 DVD File Video yang sudah di edit 10 – 30 Menit Full HD";
const String PAKET3 = "\n-1 Hari Dokumentasi\n-4 Fotografer\n-3 Videografer\n-3 Album Foto Landsacpe 20x30cm (10 Sheet/20 Halaman) \n+ Box Custom dan Tote Bag1 DVD File Foto (Soft Copy)\n-1 DVD File Video yang sudah di edit 10 – 30 Menit Full HD";
