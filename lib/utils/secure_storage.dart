import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class SecureStorage {
  static SecureStorage _instance;

  factory SecureStorage() =>
      _instance ??= SecureStorage._(FlutterSecureStorage());

  SecureStorage._(this._storage);

  final FlutterSecureStorage _storage;
  static const _tokenKey = 'TOKEN';
  static const _roleKey = 'ROLE';
  static const _descKey = 'DESC';
  static const _imageKey = 'IMAGE';
  static const _emailKey = 'EMAIL';
  static const _nameKey = 'NAME';
  static const _noKey = 'NO';
  static const _idUserKey = 'ID_USER';

  Future<void> persistEmailAndToken(String email, String token,String id,String name, String phoneNumber,String role,String image,String desc) async {
    await _storage.write(key: _emailKey, value: email);
    await _storage.write(key: _tokenKey, value: token);
    await _storage.write(key: _idUserKey, value: id);
    await _storage.write(key: _nameKey, value: name);
    await _storage.write(key: _noKey, value: phoneNumber);
    await _storage.write(key: _roleKey, value: role);
    await _storage.write(key: _imageKey, value: image);
    await _storage.write(key: _descKey, value: desc);
  }


  Future<void> updateProfile(String name, String email,String no) async {
    await _storage.write(key: _emailKey, value: email);
    await _storage.write(key: _nameKey, value: name);
    await _storage.write(key: _noKey, value: no);
   }

  Future<void> updateBio(String desc) async {
    await _storage.write(key: _descKey, value:desc);
   }

  Future<void> updateAvatar(String photo) async {
    await _storage.write(key: _imageKey, value: photo);
   }

  Future<bool> hasToken() async {
    var value = await _storage.read(key: _tokenKey);
    return value != null;
  }

  Future<bool> hasEmail() async {
    var value = _storage.read(key: _emailKey);
    return value != null;
  }

  Future<void> deleteToken() async {
    return _storage.delete(key: _tokenKey);
  }

  Future<void> deleteEmail() async {
    return _storage.delete(key: _emailKey);
  }

  Future<String> getEmail() async {
    return _storage.read(key: _emailKey);
  }

  Future<String> getName() async {
    return _storage.read(key: _nameKey);
  }

  Future<String> getToken() async {
    return _storage.read(key: _tokenKey);
  }

  Future<String> getNo() async {
    return _storage.read(key: _noKey);
  }

  Future<String> getRole() async {
    return _storage.read(key: _roleKey);
  }

 Future<String> getDesc() async {
    return _storage.read(key: _descKey);
  }


  Future<String> getImage() async {
    return _storage.read(key: _imageKey);
  }

  Future<String> getIdUser() async {
    return _storage.read(key:_idUserKey);
  }

  Future<void> deleteAll() async {
    return _storage.deleteAll();
  }
}
