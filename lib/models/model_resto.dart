/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class ModelResto{
  int id;
  String name;
  String phoneNumber;
  String email;
  int isUser;
  String workSchedule;
  int categoryId;
  String description;
  String photo;
  DateTime createdAt;
  int jml;
  int click;

  ModelResto({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isUser,
    this.workSchedule,
    this.categoryId,
    this.description,
    this.photo,
    this.createdAt,
    this.jml,
    this.click,
});


}