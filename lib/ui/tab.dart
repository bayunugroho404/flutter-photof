import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photof/ui/screens/home_screen.dart';
import 'package:photof/ui/screens/profile_screen.dart';
import 'package:photof/utils/constant.dart';



class TabScreen extends StatefulWidget {

  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  int selectedIndex = 0;
  final tabs = [
    HomeScreen(),
    ProfileScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        key: _bottomNavigationKey,
        child: Container(
          margin: EdgeInsets.only(left: 50.0, right: 50.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                onPressed: () {
                  updateTabSelection(0, "Home");
                },
                iconSize: 23.0,
                icon: Icon(
                  CupertinoIcons.home,
                  color: selectedIndex == 0
                      ? Colors.black
                      : Colors.grey.shade400,
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              IconButton(
                  onPressed: () {
                    updateTabSelection(1, "Settings");
                  },
                  iconSize: 23.0,
                  icon: Icon(
                    CupertinoIcons.gear_alt_fill,
                    color: selectedIndex == 1
                        ? Colors.black
                        : Colors.grey.shade400,
                  )),
            ],
          ),
        ),
        shape: CircularNotchedRectangle(),
        color: Colors.white,
      ),
      body: tabs[selectedIndex],

    );
  }
  void updateTabSelection(int index, String buttonText) {
    setState(() {
      selectedIndex = index;
    });
  }
}
