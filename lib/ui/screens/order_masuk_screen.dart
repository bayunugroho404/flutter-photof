import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/network/response/response_get_order_user.dart';
import 'package:photof/ui/widgets/original_button.dart';
import 'package:photof/ui/widgets/toast.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';



class OrderMasukKuScreen extends StatefulWidget {
  @override
  _OrderMasukKuScreenState createState() => _OrderMasukKuScreenState();
}

class _OrderMasukKuScreenState extends State<OrderMasukKuScreen> {
  final secure = SecureStorage();
  List<Order> listOrder = [];
  final format = new DateFormat('dd-MM-yyyy hh:mm:ss');

  @override
  void initState() {
    getDate();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text(''),
        centerTitle: true,
      ),
      body: listOrder.length > 0
          ? ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: listOrder.length,
        itemBuilder: (context,
            index,) {
          return ExpansionTile(
            title: Row(
              children: [
                Text("${formatDate(listOrder[index].createdAt)}"),
              ],
            ),
            subtitle: Text("Status : ${listOrder[index].status}"),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: listOrder[index].status == "pending" ? Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: SizeConfig.screenWidth /3,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: OriginalButton(
                              text: 'Terima',
                              color: Colors.green,
                              textColor: white,
                              onPressed: () {
                                AwesomeDialog(
                                  context: context,
                                  dialogType: DialogType.SUCCES,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: 'Apakah anda yakin ingin menerima pesanan ?',
                                  desc: '',
                                  btnCancelOnPress: (){},
                                  btnOkOnPress: () {
                                    updateStatus('success',listOrder[index].id.toString());
                                  },
                                )..show();
                                },
                            ),
                          ),
                        ),
                        Container(
                          width: SizeConfig.screenWidth /3,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: OriginalButton(
                              text: 'Tolak',
                              color: Colors.red,
                              textColor: white,
                              onPressed: () {
                                AwesomeDialog(
                                  context: context,
                                  dialogType: DialogType.ERROR,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: 'Apakah anda yakin ingin menolak pesanan ?',
                                  desc: '',
                                  btnCancelOnPress: (){},
                                  btnOkOnPress: () {
                                    updateStatus('failed',listOrder[index].id.toString());
                                  },
                                )..show();

                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Paket ${listOrder[index].packet.toString()}'),
                        SizedBox(width: 4,),
                        IconButton(icon: Icon(Icons.info_outline,color: Colors.blue,), onPressed: (){
                          AwesomeDialog(
                            context: context,
                            dialogType: DialogType.INFO,
                            animType: AnimType.BOTTOMSLIDE,
                            // ignore: unrelated_type_equality_checks
                            title: '${listOrder[index].packet == "1"?PAKET1:listOrder[index].packet == "2"?PAKET2:PAKET3}',
                            body:Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('$PAKET1'),
                            ),
                            desc: '',
                            btnOkOnPress: () {

                            },
                          )..show();
                        })
                      ],
                    ),
                  ],
                ):Center(child: Column(
                  children: [
                    Text(listOrder[index].status),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Paket ${listOrder[index].packet.toString()}'),
                        SizedBox(width: 4,),
                        IconButton(icon: Icon(Icons.info_outline,color: Colors.blue,), onPressed: (){
                          AwesomeDialog(
                            context: context,
                            dialogType: DialogType.INFO,
                            animType: AnimType.BOTTOMSLIDE,
                            // ignore: unrelated_type_equality_checks
                            title: '${listOrder[index].packet == "1"?PAKET1:listOrder[index].packet == "2"?PAKET2:PAKET3}',
                            body:Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('$PAKET1'),
                            ),
                            desc: '',
                            btnOkOnPress: () {

                            },
                          )..show();
                        })
                      ],
                    ),
                  ],
                )),
              ),
            ],
          );
        },
      )
          : Container(),
    );
  }

//fungsi untuk ambil data order berdasarkan userId login
  void getDate() async{
    listOrder.clear();
    String userId = await secure.getIdUser();
    String token = await secure.getToken();
    HomeProvider().getOrderPhotog(userId,token).then((val) {
      setState(() {
        listOrder = val.orders;
      });
    }).catchError((_) {});
  }
  //fungsi untuk format tanggal
  String formatDate(DateTime date){
    String dateFormat = format.format(date);
    return dateFormat.toString();
  }

  //fungsi untuk mengganti status orderan masuk
  void updateStatus(String title, String order_id) async{
    String token = await secure.getToken();
    HomeProvider().updateOrder(order_id, title, token).then((val){
      if(val.message == "successfull change status order"){
        showToast("sukses merubah status");
        getDate();
      }else{
        showToast("gagal merubah status");
      }
    }).catchError((onError){
      showToast('$onError');
    });
  }

}
