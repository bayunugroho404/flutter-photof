import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/network/response/response_get_review.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';



class ReviewScreen extends StatefulWidget {
  String name = '';
  String id = '';

  ReviewScreen({this.name, this.id});

  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  List<Review> listReview = [];
  final format = new DateFormat('dd-MM-yyyy hh:mm:ss');
  final secure = SecureStorage();

  @override
  void initState() {
    getDataReview();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text('Review ${widget.name !=null ?widget.name:''}'),
        centerTitle: true,
      ),
      body: listReview.length > 0
          ? ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: listReview.length,
        itemBuilder: (context,
            index,) {
          return ListTile(
            title: Text("Photofrafer : ${listReview[index].photografer.name}"),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${listReview[index].user.name} "${listReview[index].description}"'),
                RatingBar.builder(
                  initialRating: double.parse(
                      listReview[index].rate.toString()),
                  minRating: 1,
                  itemSize: 15,
                  direction: Axis.horizontal,
                  allowHalfRating: false,
                  itemCount: 5,
                  itemPadding:
                  EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) =>
                      Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                  onRatingUpdate: (rating) {},
                )
              ],
            ),
            isThreeLine: true,
            trailing: Text(
                formatDate(listReview[index].createdAt)),
          );
        },
      )
          : Container(),
    );
  }

  //fungsi untuk ambil data review berdasarakan id user login
  void getDataReview() async {
    String userId = await secure.getIdUser();
    String token = await secure.getToken();
    if (widget.name == null) {
      HomeProvider().getReviewUser(token, userId).then((val) {
        setState(() {
          listReview = val.reviews;
        });
      }).catchError((_) {});
    } else {
      HomeProvider()
          .getReviewPhotof(token, widget.id)
          .then((val) {
        setState(() {
          listReview = val.reviews;
        });
      }).catchError((_) {});
    }

  }
  String formatDate(DateTime date){
    String dateFormat = format.format(date);
    return dateFormat.toString();
  }
  }
