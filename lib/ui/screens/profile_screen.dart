import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/ui/screens/order_ku_screen.dart';
import 'package:photof/ui/screens/order_masuk_screen.dart';
import 'package:photof/ui/screens/review_screen.dart';
import 'package:photof/ui/screens/update_avatar_screen.dart';
import 'package:photof/ui/widgets/toast.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';



class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final secure = SecureStorage();
  String name='';
  String email='';
  String no='';
  String image ='';
  String token ='';
  String role ='';

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController noController = TextEditingController();

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text('Profile'),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              image!= ""?Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  children: [
                    ClipOval(
                      child: Image.network(
                        image,
                        width: SizeConfig.screenWidth * 0.3,
                        height: SizeConfig.screenHight /7,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                        top: SizeConfig.screenHight /11,
                        left: SizeConfig.screenWidth * 0.1 + 20,
                        child: IconButton(icon: Icon(Icons.edit,color: primary,size: 40,), onPressed: (){
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => UpdateAvatarScreen(
                          )));
                        }))
                  ],
                ),
              ):Container(),
              Padding(
                padding: const EdgeInsets.only(bottom: 5.0, right: 30, left: 30),
                child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 10.0,
                              offset: Offset(4.0, 4.0),
                              spreadRadius: 1.0)
                        ]),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                  margin: const EdgeInsets.only(
                                      left: 10.0, right: 20.0),
                                  child: Divider(
                                    color: Colors.black,
                                    height: 36,
                                  )),
                            ),
                            Text("Profile"),
                            Expanded(
                              child: new Container(
                                  margin: const EdgeInsets.only(
                                      left: 20.0, right: 10.0),
                                  child: Divider(
                                    color: Colors.black,
                                    height: 36,
                                  )),
                            ),
                          ],
                        ),
                        // Expanded(child: Divider()),
                        ListTile(
                          leading: Icon(CupertinoIcons.person),
                          title: Text('FullName'),
                          subtitle: Text('$name'),
                          onTap: () {},
                        ),
                        ListTile(
                          leading: Icon(CupertinoIcons.mail),
                          title: Text('Email'),
                          subtitle: Text('$email'),
                          onTap: () {},
                        ),
                        ListTile(
                          leading: Icon(CupertinoIcons.tray),
                          title: Text('Phone Number'),
                          subtitle: Text('$no'),
                          onTap: () {},
                        ),
                        ListTile(
                          leading: Icon(Icons.edit),
                          title: Text('Edit Profile'),
                          onTap: () {
                            AwesomeDialog(
                                context: context,
                                animType: AnimType.SCALE,
                                dialogType: DialogType.QUESTION,
                                body: Center(
                                  child: Column(
                                    children: [
                                      Text(
                                        'Update Profile',
                                        style: TextStyle(fontStyle: FontStyle.italic),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TextField(
                                          keyboardType: TextInputType.multiline,
                                          controller: nameController,
                                          decoration: InputDecoration(
                                            labelText: "name",
                                              contentPadding: EdgeInsets.fromLTRB(
                                                  20.0, 15.0, 20.0, 15.0),
                                              hintText: "...",
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(10.0))),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TextField(
                                          keyboardType: TextInputType.multiline,
                                          controller: emailController,
                                          decoration: InputDecoration(
                                              labelText: "email",
                                              contentPadding: EdgeInsets.fromLTRB(
                                                  20.0, 15.0, 20.0, 15.0),
                                              hintText: "...",
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(10.0))),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TextField(
                                          keyboardType: TextInputType.multiline,
                                          controller: noController,
                                          decoration: InputDecoration(
                                              labelText: "Number phone",
                                              contentPadding: EdgeInsets.fromLTRB(
                                                  20.0, 15.0, 20.0, 15.0),
                                              hintText: "...",
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(10.0))),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                title: 'This is Ignored',
                                desc: 'This is also Ignored',
                                btnOkOnPress: () {
                                  HomeProvider().updateProfile(nameController.text, noController.text, emailController.text, token).then((val){
                                    if(val.message == "OK"){
                                      showToast("Profile update Successfull");
                                      secure.updateProfile(nameController.text,emailController.text, noController.text);
                                      getUser();
                                    }
                                  });
                                },
                                btnOkText: "Update",
                                btnCancelOnPress: () {

                                })
                              ..show();
                          },
                        ),
                      ],
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 5.0, right: 30, left: 30),
                child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 10.0,
                              offset: Offset(4.0, 4.0),
                              spreadRadius: 1.0)
                        ]),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                  margin: const EdgeInsets.only(
                                      left: 10.0, right: 20.0),
                                  child: Divider(
                                    color: Colors.black,
                                    height: 36,
                                  )),
                            ),
                            Text("Activity"),
                            Expanded(
                              child: new Container(
                                  margin: const EdgeInsets.only(
                                      left: 20.0, right: 10.0),
                                  child: Divider(
                                    color: Colors.black,
                                    height: 36,
                                  )),
                            ),
                          ],
                        ),
                        // Expanded(child: Divider()),
                        ListTile(
                          leading: Icon(CupertinoIcons.tag),
                          title: Text('Ulasanku'),
                          trailing: Icon(CupertinoIcons.chevron_right),
                          onTap: () {
                            Navigator.push(
                                context, MaterialPageRoute(builder: (context) => ReviewScreen()));
                          },
                        ),
                        Visibility(
                          visible: role =="0"?true:false,
                          child: ListTile(
                            leading: Icon(CupertinoIcons.link),
                            title: Text('Order Masuk'),
                            trailing: Icon(CupertinoIcons.chevron_right),
                            onTap: () {
                              Navigator.push(
                                  context, MaterialPageRoute(builder: (context) => OrderMasukKuScreen()));
                            },
                          ),
                        ),
                        ListTile(
                          leading: Icon(CupertinoIcons.doc),
                          title: Text('Orderku'),
                          trailing: Icon(CupertinoIcons.chevron_right),
                          onTap: () {
                            Navigator.push(
                                context, MaterialPageRoute(builder: (context) => OrderKuScreen()));
                          },
                        ),
                      ],
                    )),
              ),

              Padding(
                padding: const EdgeInsets.only(bottom: 5.0, right: 30, left: 30),
                child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 10.0,
                              offset: Offset(4.0, 4.0),
                              spreadRadius: 1.0)
                        ]),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ListTile(
                          leading: Icon(CupertinoIcons.square_arrow_left),
                          title: Text('Logout'),
                          onTap: () {
                            doLogout();
                          },
                        ),
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //fungsi untuk logout
  void doLogout() {
    secure.deleteAll();
    Navigator.of(context).pushReplacementNamed('login');
  }

  //fungsi mengambil data user
  void getUser() async{
      email = await secure.getEmail();
      name = await secure.getName();
      no = await secure.getNo();
      role = await secure.getRole();
      image = await secure.getImage();
      token = await secure.getToken();

      nameController.text = name;
      noController.text = no;
      emailController.text = email;

    setState(() {});
  }
}
