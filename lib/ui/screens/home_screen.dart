import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photof/models/model_resto.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/network/response/reponse_get_photo_by_photof.dart';
import 'package:photof/network/response/response_get_category.dart';
import 'package:photof/network/response/response_get_photof_by_category.dart';
import 'package:photof/ui/screens/photografer_screen.dart';
import 'package:photof/ui/screens/search_screen.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Datum> listCategory = [];
  List<ModelResto> listphotof = [];
  final CarouselController _controller = CarouselController();
  final storage = SecureStorage();
  int current = 404;
  int currentPhotof = 0;
  String token = "";
  String image = "";
  String role = "";
  String userId = "";
  String desc = "";
  String name = "";
  int idByCurrent = 0;
  int _currentIndicator = 0;
  int idByCurrentPhotof = 0;
  bool filter = false;

  @override
  void initState() {
    getCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: ListView(
        primary: true,
        shrinkWrap: true,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Home',
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  fontSize: SizeConfig.blockVertical * 2,
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth / 3.5,
              ),
              InkWell(
                onTap: () {
                  if (role == "0") {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PhotofgraferScreen(
                                  id: userId,
                                  name: name,
                                  desc: desc,
                                  photo: image,
                                )));
                  }
                },
                child: Padding(
                  padding: EdgeInsets.only(right: 8.0, top: 8.0),
                  child: ClipOval(
                    child: image == null || image == ''
                        ? Image.asset(
                            "assets/images/prof.jpg",
                            width: 50,
                            height: 50,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            image,
                            width: 50,
                            height: 50,
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            width: SizeConfig.screenWidth,
            child: Padding(
              padding: EdgeInsets.only(left: 10),
              child: Text(
                'Hi, You !',
                textAlign: TextAlign.left,
                style: GoogleFonts.lato(
                  fontSize: SizeConfig.blockVertical * 3,
                ),
              ),
            ),
          ),
          Container(
            width: SizeConfig.screenWidth,
            child: Padding(
              padding: EdgeInsets.only(left: 10),
              child: Text(
                'Temukan Fotografer yang anda butuhkan',
                textAlign: TextAlign.left,
                style: GoogleFonts.lato(
                    fontSize: SizeConfig.blockVertical * 2,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Hero(
              tag: "search",
              child: Material(
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SearchScreen()));
                  },
                  child: TextField(
                    // controller: editingController,
                    enabled: false,
                    decoration: InputDecoration(
                        labelText: "Cari fotografer",
                        hintText: "...",
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(25.0)))),
                  ),
                ),
              ),
            ),
          ),
          Row(
            children: [
              Container(
                width: SizeConfig.screenWidth / 4,
                child: Container(
                  width: SizeConfig.screenWidth,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'Kategori',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.lato(
                          fontSize: SizeConfig.blockVertical * 3,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Container(
                width: SizeConfig.screenWidth / 1.4,
                height: SizeConfig.screenHight / 10,
                child: listCategory.length > 0
                    ? ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: listCategory.length,
                        shrinkWrap: true,
                        primary: false,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                current = index;
                                idByCurrent = listCategory[index].id;
                                getPhotofByCategory(idByCurrent);
                              });
                            },
                            child: Center(
                              child: Card(
                                color: current == index ? primary : secondary,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Text("${listCategory[index].name}"),
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : Container(),
              ),
            ],
          ),
          Container(
            width: SizeConfig.screenWidth,
            child: Padding(
              padding: EdgeInsets.only(left: 12, top: 40, bottom: 4),
              child: Row(
                children: [
                  Text(
                    'Pilih Fotografer',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.lato(
                        fontSize: SizeConfig.blockVertical * 3,
                        fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  Visibility(
                    visible: filter,
                    child: InkWell(
                      onTap: () {
                        AwesomeDialog(
                          context: context,
                          dialogType: DialogType.NO_HEADER,
                          animType: AnimType.BOTTOMSLIDE,
                          body: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  elevation: 4,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            listphotof.sort((a, b) =>
                                                a.jml.compareTo(b.jml));
                                          });
                                        },
                                        child: Text("Photo terbanyak")),
                                  ),
                                ),
                                Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  elevation: 4,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            listphotof.sort((a, b) =>
                                                b.jml.compareTo(a.jml));
                                          });
                                        },
                                        child: Text("Photo terdikit")),
                                  ),
                                ),
                                Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  elevation: 4,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            listphotof.sort((a, b) =>
                                                a.click.compareTo(b.click));
                                          });
                                        },
                                        child: Text("Rekomendasi untuk anda")),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          title: '',
                          desc: '',
                          btnOkOnPress: () {},
                        )..show();
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: Row(
                          children: [
                            Icon(Icons.filter_alt),
                            Text(
                              'Filter',
                              textAlign: TextAlign.left,
                              style: GoogleFonts.lato(
                                  fontSize: SizeConfig.blockVertical * 3,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            width: SizeConfig.screenWidth / 1.1,
            height: SizeConfig.screenHight / 3,
            child: Container(
              margin: EdgeInsets.only(left: 4, right: 4),
              child: listphotof.length > 0
                  ? ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: listphotof.length,
                      shrinkWrap: true,
                      primary: false,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PhotofgraferScreen(
                                          id: listphotof[index].id.toString(),
                                          name: listphotof[index].name,
                                          desc: listphotof[index].description,
                                          photo: listphotof[index].photo,
                                        )));
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            elevation: 4,
                            child: Container(
                              width: SizeConfig.screenWidth / 1.6,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image.network(
                                    listphotof[index].photo,
                                    width: SizeConfig.screenWidth,
                                    height: SizeConfig.screenHight / 5,
                                    fit: BoxFit.fill,
                                  ),
                                  SizedBox(height: 10),
                                  Padding(
                                    padding: EdgeInsets.only(left: 4),
                                    child: Text(
                                      '${listphotof[index].name}',
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.lato(
                                          fontSize:
                                              SizeConfig.blockVertical * 2.5,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 4, top: 4),
                                    child: Text(
                                      '${listphotof[index].description}',
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.lato(
                                          fontSize:
                                              SizeConfig.blockVertical * 2),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    )
                  : Center(
                      child: Text('data tidak ada'),
                    ),
            ),
          ),
        ],
      ),
    );
  }

  //fungsi untuk ambil data kategori
  void getCategory() async {
    token = await storage.getToken();
    image = await storage.getImage();
    name = await storage.getName();
    role = await storage.getRole();
    desc = await storage.getDesc();
    userId = await storage.getIdUser();
    setState(() {});
    await HomeProvider().getCategory().then((val) {
      setState(() {
        listCategory = val.data;
        idByCurrent = val.data[0].id;
      });
      if (val.data.length > 0) {
        getPhotofByCategory(val.data[0].id);
      }
    }).catchError((onError) {});
  }

  //fungsi untuk ambil data foto berdasarkan category id
  void getPhotofByCategory(int idByCurrent) {
    listphotof.clear();
    print('masuk');

    HomeProvider().getPhotoByCategory(idByCurrent, token).then((val) {
      print(val.statusCode);
      val.photografer.forEach((element) {
        setState(() {
          listphotof.add(new ModelResto(
            name: element.name,
            id: element.id,
            email: element.email,
            isUser: int.parse(element.isUser.toString()),
            workSchedule: element.workSchedule,
            categoryId: element.id,
            description: element.description,
            photo: element.photo,
            createdAt: element.createdAt,
            jml: element.photos.length,
            click: int.parse(element.click.toString()),
          ));
        });
      });
      if (val.photografer.length < 1) {
        setState(() {
          filter = false;
        });
        Future.delayed(Duration(seconds: 1), () {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'tidak ada Fotografer',
            desc: '',
            btnOkOnPress: () {},
          )..show();
        });
      } else {
        setState(() {
          filter = true;
        });
      }
    }).catchError((onError) {});
  }
}
