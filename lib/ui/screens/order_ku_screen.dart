import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/network/response/response_get_order_user.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';



class OrderKuScreen extends StatefulWidget {
  @override
  _OrderKuScreenState createState() => _OrderKuScreenState();
}

class _OrderKuScreenState extends State<OrderKuScreen> {
  final secure = SecureStorage();
  List<Order> listOrder = [];
  final format = new DateFormat('dd-MM-yyyy hh:mm:ss');

  @override
  void initState() {
    getDate();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text(''),
        centerTitle: true,
      ),
      body: listOrder.length > 0
          ? ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: listOrder.length,
        itemBuilder: (context,
            index,) {
          return ListTile(
            title: Text("Photofrafer : ${listOrder[index].photografer.name}"),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${listOrder[index].status}'),
                SizedBox(height: 10,),
                Row(
                  children: [
                    Text('Paket ${listOrder[index].packet.toString()}'),
                    SizedBox(width: 4,),
                    IconButton(icon: Icon(Icons.info_outline,color: Colors.blue,), onPressed: (){
                      AwesomeDialog(
                        context: context,
                        dialogType: DialogType.INFO,
                        animType: AnimType.BOTTOMSLIDE,
                        // ignore: unrelated_type_equality_checks
                        title: '${listOrder[index].packet == "1"?PAKET1:listOrder[index].packet == "2"?PAKET2:PAKET3}',
                        body:Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('$PAKET1'),
                        ),
                        desc: '',
                        btnOkOnPress: () {

                        },
                      )..show();
                    })
                  ],
                ),
              ],
            ),
            isThreeLine: true,
            trailing: Text(
                formatDate(listOrder[index].createdAt)),
          );
        },
      )
          : Container(),
    );
  }
  //fungsi untuk ambil data order
  void getDate() async{
    String userId = await secure.getIdUser();
    String token = await secure.getToken();
    HomeProvider().getOrderUser(userId,token).then((val) {
      setState(() {
        listOrder = val.orders;
      });
    }).catchError((_) {});
  }
  String formatDate(DateTime date){
    String dateFormat = format.format(date);
    return dateFormat.toString();
  }

}
