import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photof/ui/widgets/original_button.dart';
import 'package:photof/utils/size_config.dart';


//screen untuk aplikasi pertama dibuka
class IntroScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Color(0xffC4C4C4),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(),
            Hero(
              tag: 'logoAnimation',
              child: Image.asset(
                'assets/images/saass.png',
                fit: BoxFit.fill,
                height: MediaQuery.of(context).size.height /5,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Temukan resep, ide rumah, inspirasi gaya, dan ide lain untuk dicoba.',
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                    fontSize: SizeConfig.blockVertical * 3,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: OriginalButton(
                text: 'Get Started',
                color: Colors.white,
                textColor: Colors.lightBlue,
                onPressed: () {
                  // Navigator.of(context).pushNamed('login');
                  Navigator.pushNamedAndRemoveUntil(
                      context, 'login', ModalRoute.withName('intro'));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
