import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/network/response/reponse_get_photo_by_photof.dart' as poto;
import 'package:photof/network/response/response_get_photof_by_category.dart';
import 'package:photof/ui/screens/add_image_screen.dart';
import 'package:photof/ui/screens/order_screen.dart';
import 'package:photof/ui/screens/review_screen.dart';
import 'package:photof/ui/widgets/image_detail.dart';
import 'package:photof/ui/widgets/toast.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';



class PhotofgraferScreen extends StatefulWidget {
  String name;
  String id;
  String desc;
  String photo;

  PhotofgraferScreen({this.name,this.id,this.desc,this.photo});

  @override
  _PhotofgraferScreenState createState() => _PhotofgraferScreenState();
}

class _PhotofgraferScreenState extends State<PhotofgraferScreen> {
  List<poto.Photo> listphotoByPhotof = [];
  final storage = SecureStorage();
  String rate = '3';
  String token = '';
  String user_id = '';
  TextEditingController reviewTextController = TextEditingController();
  TextEditingController bioController = TextEditingController();

  @override
  void initState() {
    getPhotoByPhotof(widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Photografer Detail"),
        backgroundColor: primary,
      ),
      floatingActionButton: Visibility(
        visible: user_id == widget.id ?false:true,
        child: FloatingActionButton.extended(
          label: Text('Pesan'),
          icon: Icon(Icons.present_to_all),
          backgroundColor: primary,
          foregroundColor: Colors.white,
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => OrderScreen(
                id:widget.id,
                name:widget.name
            )));
          },
        ),
      ),
      body: ListView(
        primary: true,
        shrinkWrap: true,
        children: [
          Container(
            height: SizeConfig.screenHight / 4,
            width: SizeConfig.screenWidth,
            margin: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                      child: Image.network("${widget.photo}",
                        width: 100,
                        height: 100,
                      ),
                    ),
                    SizedBox(width: 70,),
                    Column(
                      children: [
                        Visibility(
                          visible: user_id == widget.id.toString() ?false:true,
                          child: ElevatedButton(
                            onPressed: () {
                              AwesomeDialog(
                                  context: context,
                                  animType: AnimType.SCALE,
                                  dialogType: DialogType.QUESTION,
                                  body: Center(
                                    child: Column(
                                      children: [
                                        Text(
                                          'Kirim Ulasan',
                                          style: TextStyle(fontStyle: FontStyle.italic),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        RatingBar.builder(
                                          initialRating: 3,
                                          minRating: 1,
                                          itemSize: 30,
                                          direction: Axis.horizontal,
                                          allowHalfRating: false,
                                          itemCount: 5,
                                          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Colors.amber,
                                          ),
                                          onRatingUpdate: (rating) {
                                              rate = rating.toInt().toString();
                                            },
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: 4,
                                            controller: reviewTextController,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.fromLTRB(
                                                    20.0, 15.0, 20.0, 15.0),
                                                hintText: "...",
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(10.0))),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  title: 'This is Ignored',
                                  desc: 'This is also Ignored',
                                  btnOkOnPress: () {
                                    if(!reviewTextController.text.isEmpty){
                                      HomeProvider().postReview(token, rate, user_id, reviewTextController.text, widget.id.toString()).then((val){
                                        if(val.reviews =="successfull review"){
                                          showToast("sukses kirim ulasan");
                                        }else{
                                          showToast("gagal kirim ulasan");
                                        }
                                      }).catchError((onError){
                                        showToast("$onError");
                                      });
                                    }else{
                                      showToast("ulasan tidak boleh kosong");
                                    }
                                    // controller.postReview(controller.rate.value,
                                    //     controller.reviewTextController.text, id.toString());
                                  },
                                  btnOkText: "Submit",
                                  btnCancelOnPress: () {

                                  })
                                ..show();
                            },
                            child: Text('Kirim ulasan'),
                            style: ElevatedButton.styleFrom(shape: StadiumBorder(), primary: grey),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context, MaterialPageRoute(builder: (context) => ReviewScreen(
                              name: widget.name,
                              id: widget.id.toString(),
                            )));
                          },
                          child: Text('Lihat ulasan'),
                          style: ElevatedButton.styleFrom(shape: StadiumBorder(), primary: grey),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(height: 20,),
                Text(
                  '${widget.name}',
                  textAlign: TextAlign.left,
                  style: GoogleFonts.lato(
                      fontSize:
                      SizeConfig.blockVertical * 2,
                      color: Colors.black87,
                  fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 4,),
                Expanded(
                  child: Row(
                    children: [
                      Text(
                        '${widget.desc == null ? 'tidak ada bio':widget.desc}',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.lato(
                            fontSize:
                            SizeConfig.blockVertical * 2,
                            color: grey),
                      ),
                      Visibility(
                        visible: widget.id == user_id ?true:false,
                        child: IconButton(icon: Icon(Icons.edit), onPressed: (){
                          AwesomeDialog(
                              context: context,
                              animType: AnimType.SCALE,
                              dialogType: DialogType.QUESTION,
                              body: Center(
                                child: Column(
                                  children: [
                                    Text(
                                      'Update Bio',
                                      style: TextStyle(fontStyle: FontStyle.italic),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                        keyboardType: TextInputType.multiline,
                                        maxLines: 4,
                                        controller: bioController,
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                20.0, 15.0, 20.0, 15.0),
                                            hintText: "...",
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                BorderRadius.circular(10.0))),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              title: 'This is Ignored',
                              desc: 'This is also Ignored',
                              btnOkOnPress: () {
                                if(!bioController.text.isEmpty){
                                  HomeProvider().updateBio( bioController.text,token).then((val){
                                    if(val.message =="OK"){
                                      storage.updateBio(bioController.text);
                                      showToast("sukses update bio");
                                      Navigator.of(context).pushReplacementNamed('home');
                                    }else{
                                      showToast("gagal update bio");
                                    }
                                  }).catchError((onError){
                                    showToast("$onError");
                                  });
                                }else{
                                  showToast("bio tidak boleh kosong");
                                }
                              },
                              btnOkText: "Submit",
                              btnCancelOnPress: () {

                              })
                            ..show();
                        }),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: user_id == widget.id.toString() ?true:false,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => AddImageScreen(
                    name: widget.name,
                    id: widget.id.toString(),
                    refresh: getPhotoByPhotof(widget.id),
                  ))).then((value) {
                    getPhotoByPhotof(widget.id);
                  });
                },
                child: Text('Tambah gambar'),
                style: ElevatedButton.styleFrom(shape: StadiumBorder(), primary: grey),
              ),
            ),
          ),
          listphotoByPhotof.length > 0
              ? ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: listphotoByPhotof.length ,
              itemBuilder: (BuildContext context, int index) {
                return Hero(
                  tag: "photo" + index.toString(),
                  child: Material(
                    child: InkWell(
                      onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ImageDetail(
                              path: listphotoByPhotof[index].photo,
                              tag:"photo" + index.toString()
                          )));},
                      child: Stack(children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(
                                "${listphotoByPhotof[index].photo}",
                              ),
                            ),
                          ),
                          height: 200.0,
                        ),
                        Container(
                          height: 200.0,
                          width: SizeConfig.screenWidth,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              gradient: LinearGradient(
                                  begin: FractionalOffset.topCenter,
                                  end: FractionalOffset.bottomCenter,
                                  colors: [
                                    Colors.transparent,
                                    Colors.white,
                                  ],
                                  stops: [
                                    0.0,
                                    2.0
                                  ])),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                            "",
                                            style: GoogleFonts.lato(
                                                fontSize:
                                                SizeConfig.blockVertical * 3,
                                                fontWeight: FontWeight.bold)),
                                        Text(
                                            "",
                                            style: GoogleFonts.lato(
                                                fontSize:
                                                SizeConfig.blockVertical * 2,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    Spacer(),

                                  ],
                                )),
                          ),
                        )
                      ]),
                    ),
                  ),
                );
              })
              : Container()
        ],
      ),
    );
  }
  //fungsi untuk ambil semua foto berdasarkan photografer id
  Future<bool> getPhotoByPhotof(String id) async{
    listphotoByPhotof.clear();
    token = await storage.getToken();
    user_id = await storage.getIdUser();
    setState(() {});
    HomeProvider().getPhotoByPhotof(id, token).then((val) {
      setState(() {
        listphotoByPhotof = val.photo;
      });
    }).catchError((onError) {});
    return true;

  }

}
