

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/network/response/response_photografer.dart';
import 'package:photof/ui/screens/photografer_screen.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  TextEditingController searchController = new TextEditingController();
  final key = new GlobalKey<ScaffoldState>();
  final storage = SecureStorage();
  List <ResponsePhotograferPhoto> listPG=[];
  String filter = '';

  @override
  void initState() {
    getPhotografer();
    searchController.addListener(() {
      setState(() {
        filter = searchController.text;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios,color: primary,), onPressed: () {
            Navigator.pop(context);
        },
        ),
        title:  Hero(
          tag: "search",
          child: Material(
            child: Container(
                height: 50,
                padding: EdgeInsets.all(8),
                child: TextField(
                  autofocus: true,
                    controller: searchController,
                    decoration: InputDecoration(
                        fillColor: Colors.black.withOpacity(0.1),
                        filled: true,
                        prefixIcon: Icon(Icons.search),
                        hintText: 'Cari fotografer ...',
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10), borderSide: BorderSide.none),
                        contentPadding: EdgeInsets.zero
                    )
                )
            ),
          ),
        ),
      ),
      body:  listPG.length > 0
          ? ListView.builder(
        itemCount: listPG.length,
        itemBuilder: (BuildContext context, int index) {
          return filter == null || filter == ""
              ? Container()
              : '${listPG[index].name}'
              .toLowerCase()
              .contains(filter.toLowerCase())
              ? InkWell(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => PhotofgraferScreen(
                id:listPG[index].id.toString(),
                name:listPG[index].name,
                desc:listPG[index].description,
                photo:listPG[index].photo,
              )));
            },
            child: Container(
              margin: EdgeInsets.all(8),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                clipBehavior: Clip.antiAliasWithSaveLayer,
                elevation: 4,
                child: Container(
                  width: SizeConfig.screenWidth / 1.6,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.network(
                        listPG[index].photo,
                        width: SizeConfig.screenWidth,
                        height: SizeConfig.screenHight / 5,
                        fit: BoxFit.cover,
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.only(left: 4),
                        child: Text(
                          '${listPG[index].name}',
                          textAlign: TextAlign.left,
                          style: GoogleFonts.lato(
                              fontSize:
                              SizeConfig.blockVertical * 2.5,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 4,top: 4,bottom: 8),
                        child: Text(
                          '${listPG[index].description}',
                          textAlign: TextAlign.left,
                          style: GoogleFonts.lato(
                              fontSize:
                              SizeConfig.blockVertical * 2),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
              : Container();
        },
      )
          : Center(child: CircularProgressIndicator()),
    );
  }

  //fungsi ambil semua data photografer
  void getPhotografer() async{
    String token = await storage.getToken();
    listPG.clear();
    HomeProvider().getPhotografer(token).then((value) {
      setState(() {
        listPG = value.photo;
      });
    });
  }
}
