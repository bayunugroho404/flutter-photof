import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photof/network/apiServiceImage.dart';
import 'package:photof/ui/widgets/toast.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';



class UpdateAvatarScreen extends StatefulWidget {

  @override
  _UpdateAvatarScreenState createState() => _UpdateAvatarScreenState();
}

class _UpdateAvatarScreenState extends State<UpdateAvatarScreen> {
  PickedFile imageFile;
  final storage = SecureStorage();
  dynamic pickImageError;
  final _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('update avatar'),
        backgroundColor: primary,
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: Column(
                  children: [
                    imageFile != null ?Semantics(
                        child: Image.file(File(imageFile.path)),
                        label: 'image_picker_example_picked_image'):Text('Image not selected'),
                    ListTile(
                      title: Padding(
                        padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                        child: new RaisedButton(
                          child: Text(
                              "Choose Image"),
                          color: primary,
                          textColor: Colors.white,
                          onPressed: () {
                            getImageFromGallery();
                          },
                        ),
                      ),
                    ),
                    Visibility(
                      visible: imageFile != null?true:false,
                      child: ListTile(
                        title: Padding(
                          padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                          child: new RaisedButton(
                            child: Text(
                                "Upload"),
                            color: primary,
                            textColor: Colors.white,
                            onPressed: () {
                              addPhoto(imageFile);
                              // controller.addMenu(refresh!,controller.imageFile!, id.toString(),context);
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  //fungsi untuk ambil foto dari gallery
  Future getImageFromGallery() async {
    try {
      final pickedFile = await _picker.getImage(
        source: ImageSource.gallery,
      );
      imageFile = pickedFile;
      print(imageFile?.path);
      setState(() {});
    } catch (e) {
      pickImageError = e;
    }
  }

  //fungsi untuk update foto profile
  void addPhoto(PickedFile imageFile) async{
    String token = await storage.getToken();
    ApiServiceImage().updateAvatar(token, imageFile).then((val){
      if(val.message =="OK"){
        showToast("avatar update successfull");
        storage.updateAvatar(val.user.photo);
        Navigator.of(context).pushReplacementNamed('home');
      }
    }).catchError((onError){
      showToast('$onError');
      print('$onError');
    });
  }
}
