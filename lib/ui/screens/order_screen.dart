import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/ui/widgets/original_button.dart';
import 'package:photof/ui/widgets/toast.dart';
import 'package:photof/utils/constant.dart';
import 'package:photof/utils/secure_storage.dart';
import 'package:photof/utils/size_config.dart';



class OrderScreen extends StatefulWidget {
  String id;
  String name;

  OrderScreen({this.id, this.name});

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  int current = 0;
  final secure = SecureStorage();
  TextEditingController noteController = TextEditingController();
  String myDate = "select date";

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Pesan'),
        backgroundColor: primary,
      ),
      body: ListView(
        primary: true,
        shrinkWrap: true,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 8, top: 20),
            child: Text(
              'Pilih Paket',
              textAlign: TextAlign.left,
              style: GoogleFonts.lato(
                  fontSize: SizeConfig.blockVertical * 3,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            height: SizeConfig.screenHight / 3,
            width: SizeConfig.screenWidth,
            margin: EdgeInsets.all(8),
            child: ListView(
              scrollDirection: Axis.horizontal,
              primary: false,
              shrinkWrap: true,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      current = 0;
                    });
                  },
                  child: Card(
                    color: current == 0 ? primary : white,
                    child: Container(
                      margin: EdgeInsets.all(20),
                      height: SizeConfig.screenHight / 2,
                      width: SizeConfig.screenWidth / 1.5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Paket 1',
                            textAlign: TextAlign.left,
                            style: GoogleFonts.lato(
                                fontSize: SizeConfig.blockVertical * 3,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Paket 1\n-1 Hari Dokumentasi\n-2 Fotografer\n-1 Videografer\n-1 Album Foto Landsacpe 20x30cm (10 Sheet/20 Halaman) \n+ Box Custom dan Tote Bag1 DVD File Foto (Soft Copy)\n-1 DVD File Video yang sudah di edit 10 – 30 Menit Full HD',
                              textAlign: TextAlign.left,
                              style: GoogleFonts.lato(
                                fontSize: SizeConfig.blockVertical * 2,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      current = 1;
                    });
                  },
                  child: Card(
                    color: current == 1 ? primary : white,
                    child: Container(
                      margin: EdgeInsets.all(20),
                      height: SizeConfig.screenHight / 2,
                      width: SizeConfig.screenWidth / 1.5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Paket 2',
                            textAlign: TextAlign.left,
                            style: GoogleFonts.lato(
                                fontSize: SizeConfig.blockVertical * 3,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Expanded(
                            child: Text(
                              '-1 Hari Dokumentasi\n-3 Fotografer\n-2 Videografer\n-2 Album Foto Landsacpe 20x30cm (10 Sheet/20 Halaman) \n+ Box Custom dan Tote Bag1 DVD File Foto (Soft Copy)\n-1 DVD File Video yang sudah di edit 10 – 30 Menit Full HD',
                              textAlign: TextAlign.left,
                              style: GoogleFonts.lato(
                                fontSize: SizeConfig.blockVertical * 2,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      current = 2;
                    });
                  },
                  child: Card(
                    color: current == 2 ? primary : white,
                    child: Container(
                      margin: EdgeInsets.all(20),
                      height: SizeConfig.screenHight / 2,
                      width: SizeConfig.screenWidth / 1.5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Paket 3',
                            textAlign: TextAlign.left,
                            style: GoogleFonts.lato(
                                fontSize: SizeConfig.blockVertical * 3,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Expanded(
                            child: Text(
                              '\n-1 Hari Dokumentasi\n-4 Fotografer\n-3 Videografer\n-3 Album Foto Landsacpe 20x30cm (10 Sheet/20 Halaman) \n+ Box Custom dan Tote Bag1 DVD File Foto (Soft Copy)\n-1 DVD File Video yang sudah di edit 10 – 30 Menit Full HD',
                              textAlign: TextAlign.left,
                              style: GoogleFonts.lato(
                                fontSize: SizeConfig.blockVertical * 2,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 8, top: 20),
            child: Text(
              'Tentukan Jadwal',
              textAlign: TextAlign.left,
              style: GoogleFonts.lato(
                  fontSize: SizeConfig.blockVertical * 3,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Material(
            child: TextButton(
                onPressed: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2018, 3, 5),
                      maxTime: DateTime(2025, 6, 7), onChanged: (date) {
                    print('change $date');
                    myDate = date.day.toString() +"-"+date.month.toString() +"-"+date.year.toString();
                    setState(() {});
                    }, onConfirm: (date) {
                    print('confirm $date');
                  }, currentTime: DateTime.now(), locale: LocaleType.id);
                },
                child: Text(
                  '$myDate',
                  style: TextStyle(color: Colors.blue),
                )),
          ),
          Padding(
            padding: EdgeInsets.only(left: 8, top: 20),
            child: Text(
              'Note',
              textAlign: TextAlign.left,
              style: GoogleFonts.lato(
                  fontSize: SizeConfig.blockVertical * 3,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              keyboardType: TextInputType.multiline,
              maxLines: 4,
              controller: noteController,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(
                      20.0, 15.0, 20.0, 15.0),
                  hintText: "...",
                  border: OutlineInputBorder(
                      borderRadius:
                      BorderRadius.circular(10.0))),
            ),
          ),
          OriginalButton(
            text: 'Submit',
            color: primary,
            textColor: Colors.white,
            onPressed: () {
              AwesomeDialog(
                context: context,
                dialogType: DialogType.INFO,
                animType: AnimType.BOTTOMSLIDE,
                title: 'apakah anda yakin ingin pesan ?',
                desc: '',
                btnCancelOnPress: (){},
                btnOkOnPress: () async{
                  //fungsi untuk order
                  String user_id = await secure.getIdUser();
                  String token = await secure.getToken();
                  HomeProvider().addOrder(user_id, myDate, (current+1).toString(), noteController.text, widget.id, token).then((val){
                    if(val.message == "successfull order"){
                      Navigator.of(context).pushReplacementNamed('home');
                    }else{
                      showToast("gagal pesan");
                    }
                  }).catchError((error){
                    showToast("$error");
                  });
                },
              )..show();
            },
          )
        ],
      ),
    );
  }
}
