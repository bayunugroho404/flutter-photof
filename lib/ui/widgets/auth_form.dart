import 'package:flutter/material.dart';
import 'package:photof/network/provider/home_provider.dart';
import 'package:photof/network/provider/user_provider.dart';
import 'package:photof/network/response/default_response.dart';
import 'package:photof/network/response/login_response.dart';
import 'package:photof/network/response/response_get_category.dart';
import 'package:photof/ui/screens/auth_screen.dart';
import 'package:photof/ui/widgets/animated_toggle.dart';
import 'package:photof/ui/widgets/original_button.dart';
import 'package:photof/ui/widgets/toast.dart';
import 'package:photof/utils/secure_storage.dart';

class AuthForm extends StatefulWidget {
  final AuthType authType;

  const AuthForm({Key key, @required this.authType}) : super(key: key);

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  final _formKey = GlobalKey<FormState>();
  final secure = SecureStorage();
  String _name = '',
      _email = '',
      _password = '',
      _confirmationPassword = '',
      _phone = '';
  String categoryId;
  int _toggleValue = 0;
  UserProvider _userProvider;
  List<Datum> listCategory = [];

  // AuthBase authBase = AuthBase();

  @override
  void initState() {
    _userProvider = UserProvider();
    checkUser();
    getCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 0),
        child: Column(
          children: <Widget>[
            widget.authType == AuthType.register
                ? TextFormField(
              decoration: InputDecoration(
                labelText: 'Enter your name',
                hintText: 'ex: Muhammad Alif',
              ),
              onChanged: (value) {
                _name = value;
              },
              validator: (value) => value.isEmpty || value.length <= 3
                  ? 'You must enter a valid name'
                  : null,
            )
                : Container(),
            widget.authType == AuthType.register
                ? SizedBox(height: 10)
                : Container(),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Enter your email',
                hintText: 'ex: test@gmail.com',
              ),
              onChanged: (value) {
                _email = value;
              },
              validator: (value) =>
              value.isEmpty ? 'You must enter a valid email' : null,
            ),
            SizedBox(height: 10),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Enter your password',
              ),
              obscureText: true,
              onChanged: (value) {
                _password = value;
              },
              validator: (value) => value.length <= 6
                  ? 'Your password must be larger than 6 characters'
                  : null,
            ),
            widget.authType == AuthType.register
                ? SizedBox(height: 10)
                : Container(),
            widget.authType == AuthType.register
                ? TextFormField(
              decoration: InputDecoration(
                labelText: 'Enter your confirmation password',
              ),
              obscureText: true,
              onChanged: (value) {
                _confirmationPassword = value;
              },
              validator: (value) => value.length <= 6
                  ? 'Your password must be larger than 6 characters'
                  : null,
            )
                : Container(),
            widget.authType == AuthType.register
                ? Column(
              children: [
                AnimatedToggle(
                  values: ['Photograper', 'User'],
                  onToggleCallback: (value) {
                    setState(() {
                      _toggleValue = value;
                    });
                  },
                  buttonColor: const Color(0xFF0A3157),
                  backgroundColor: const Color(0xFFB5C1CC),
                  textColor: const Color(0xFFFFFFFF),
                ),
              ],
            )
                : Container(),
            widget.authType == AuthType.register
                ? SizedBox(height: 10)
                : Container(),
            widget.authType == AuthType.register
                ? TextFormField(
              decoration: InputDecoration(
                labelText: 'Enter your phone number',
              ),
              obscureText: false,
              onChanged: (value) {
                _phone = value;
              },
              validator: (value) => value.length <= 6 ||
                  value.length >= 13
                  ? 'Your number must be larger than 6 characters and maximal 13 characters'
                  : null,
            )
                : Container(),
            widget.authType == AuthType.register
                ? Visibility(
              visible: _toggleValue == 0 ? true : false,
              child: DropdownButton(
                hint: Text("Select Category"),
                value: categoryId,
                items: listCategory.map((item) {
                  return DropdownMenuItem(
                    child: Text('${item.name}'),
                    value: item.id.toString(),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    categoryId = value;
                  });
                },
              ),
            )
                : Container(),
            SizedBox(height: 20),
            OriginalButton(
              text: widget.authType == AuthType.login ? 'Login' : 'Register',
              color: Colors.lightBlue,
              textColor: Colors.white,
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  if (widget.authType == AuthType.login) {
                    // await authBase.loginWithEmailAndPassword(_email, _password);
                    await _userProvider.login(_email, _password).then((val) {
                      if (val.statusCode == 200) {
                        secure.persistEmailAndToken(
                            _email,
                            val.accessToken,
                            val.user.id.toString(),
                            val.user.name,
                            val.user.phoneNumber,
                            val.user.isUser.toString(),
                            val.user.photo,
                            val.user.description);
                        Navigator.of(context).pushReplacementNamed('home');
                      }
                    }).catchError((onError) {
                      showToast("ops account belum terdaftar.");
                    });
                  } else {
                    if(_toggleValue == 0){
                      DefaultResponse response = await _userProvider.registerPhotof(
                          _name,
                          _email,
                          _password,
                          _confirmationPassword,
                          _phone,
                          _toggleValue,
                          categoryId
                      );
                      if (response.statusCode == 200) {
                        Navigator.of(context).pushReplacementNamed('login');
                      } else if (response.statusCode == 422) {
                        showToast(
                            "ops anda gagal terdaftar.\nsilahkan coba lagi.");
                      }
                    }else{
                      DefaultResponse response = await _userProvider.register(
                          _name,
                          _email,
                          _password,
                          _confirmationPassword,
                          _phone,
                          _toggleValue);
                      if (response.statusCode == 200) {
                        Navigator.of(context).pushReplacementNamed('login');
                      } else if (response.statusCode == 422) {
                        showToast(
                            "ops anda gagal terdaftar.\nsilahkan coba lagi.");
                      }
                    }

                  }
                }
              },
            ),
            SizedBox(height: 12),
            InkWell(
              onTap: () {
                if (widget.authType == AuthType.login) {
                  Navigator.of(context).pushReplacementNamed('register');
                } else {
                  Navigator.of(context).pushReplacementNamed('login');
                }
              },
              child: Text(
                widget.authType == AuthType.login
                    ? 'Don\'t have an account?'
                    : 'Already have an account?',
                style: TextStyle(fontSize: 18, color: Colors.black54),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void checkUser() async {
    final hasToken = await secure.hasToken();
    if (hasToken) {
      Navigator.of(context).pushReplacementNamed('home');
    }
  }

  void getCategory() async {
    HomeProvider().getCategory().then((val) {
      setState(() {
        listCategory = val.data;
      });
    }).catchError((onError) {});
  }
}
