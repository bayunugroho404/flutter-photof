import 'package:flutter/material.dart';




class ImageDetail extends StatefulWidget {
  String path;
  String tag;

  ImageDetail({ this.path,this.tag});

  @override
  _ImageDetailState createState() => _ImageDetailState();
}

class _ImageDetailState extends State<ImageDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 30),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    width: 100,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                      ),
                      onPressed: () => Navigator.of(context).pop(),
                      child: Icon(Icons.close,color: Colors.grey,),
                    ),
                  ),
                ],
              ),
              Container ( width: MediaQuery
                  .of(context)
                  .size
                  .width,
                  child:
                  Hero(
                      tag: widget.tag,
                      child: Image.network(widget.path, fit: BoxFit.fitWidth ))
              ),
            ],
          ),
        ),
      ),
    );
  }
}